# Getting Started with Create React App

This project was created for the purpose of screening with ITV

## Available Scripts

In the project directory, you can run:
### `npm start`
Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `npm test`
Launches the test runner 


## Trade Offs

Based on the information given I assumed that:
A one page application that fufills all what was asked was fine since there were no AC's
Basic CSS stylings was ok as no design template was specified
The values once a task is completed, the user may still want to be able to edit the task name or delete it fromt there list
A flag will suits the purpose of identifying if a task is completed or not
No DB  needed for the complexisty of the application
A central state management system is not needed as this a simple application
Just one user would be using this at any given time, so no need for store user token, 
Based on the above a local/session storage would do to persist user data
One Master Branch is fine as it would never be maintained
No need for Pipeline configuration for the same reason
