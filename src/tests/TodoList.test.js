import { render, screen } from '@testing-library/react';
import TodoList from '../components/TodoList';

const mockTodos = [
    {id:1, text: 'Go to the Pharmacy'},
    {id:2, text: 'Drive to work'},
    {id:3, text: 'Prepare for planning'}
]

describe('TodoList', () => {
  test('renders todo list', () => {
    render(<TodoList todos={mockTodos} 
        completeTodo={mockTodos}
        removeTodo={()=> {}}
        updateTodo={()=> {}}/>);
    const text1 = screen.getByText(/Go to the Pharmacy/i);
    expect(text1).toBeInTheDocument();
    const text2 = screen.getByText(/Drive to work/i);
    expect(text2).toBeInTheDocument();
    const text3 = screen.getByText(/Prepare for planning/i);
    expect(text3).toBeInTheDocument();
  });

  test('renders icon for edit and cancel', () => {
   const {container} = render(<TodoList todos={mockTodos} 
        completeTodo={mockTodos}
        removeTodo={()=> {}}
        updateTodo={()=> {}}/>);
    const deleteIcon = container.getElementsByClassName("delete-icon");
    expect(deleteIcon.length).toBe(3);
    const editIcon = container.getElementsByClassName("edit-icon");
    expect(editIcon.length).toBe(3);
  });
});
