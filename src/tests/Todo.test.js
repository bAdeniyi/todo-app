import { render, screen } from '@testing-library/react';
import Todo from '../components/Todo';

describe('Todo', () => {
  test('renders todo app title', () => {
    render(<Todo />);
    const titleText = screen.getByText(/Things to do today/i);
    expect(titleText).toBeInTheDocument();
  });
});
