import { render, screen } from '@testing-library/react';
import TodoForm from '../components/TodoForm';

describe('TodoForm', () => {
  test('renders default placeholder', () => {
    render(<TodoForm />);
    const placeholderText = screen.getByPlaceholderText(/add a todo/i);
    expect(placeholderText).toBeInTheDocument();
  });
  test('renders default button text', () => {
    render(<TodoForm />);
    const buttonText = screen.getByText(/Add Agenda/i);
    expect(buttonText).toBeInTheDocument();
  });
});
