import React, {useState, useEffect} from 'react'
import TodoForm from './TodoForm'
import TodoList from './TodoList'

/**
 * 
 * Handles the operational logic of the application
 * users can update their todo list, create a todo
 * remove them and also mark them as completed
 */

const Todo = () => {
    const [todos, setTodos] = useState(JSON.parse(localStorage.getItem('todo-app')) || [])

    useEffect(() => {
        localStorage.setItem('todo-app', JSON.stringify(todos));
    }, [todos]);

    const addTodo = todo => {

        // should not accept a space or an unreasonable string
        if(!todo.text || /^\s*$/.test(todo.text)) {
            return 
        }
        const newTodos = [todo, ...todos];
        setTodos(newTodos);
    }

    // todo object is updated to have a true value here when marked as completed
    const completeTodo = id => {
        let updatedTodos = todos.map(todo => {
            if(todo.id === id) {
                todo.isComplete = !todo.isComplete
            }
            return todo;
        })
        setTodos(updatedTodos)
    }

    const updateTodo = (todoId, newValue) => {
        
        // should not accept a space or an unreasonable string
        if(!newValue.text || /^\s*$/.test(newValue.text)) {
            return 
        }

        setTodos(prev => prev.map(item => (item.id === todoId ? newValue : item)))
    }

    const removeTodo = id => {
        const removeArr = [...todos].filter(todo => todo.id !== id)
        setTodos(removeArr)
    }

    return (
        <div>
            <h1>Things to do today</h1>
        
            <TodoForm onSubmit={addTodo}/>
            <TodoList
                todos={todos} 
                completeTodo={completeTodo}
                removeTodo={removeTodo}
                updateTodo={updateTodo}
            />
        </div>
    )
}

export default Todo

/**
 * TRADE OFFS
 * 
 * We are assumed that:
 * No DB  needed for the complexisty of the application
 * A central state management system is not needed as this a simple application
 * Just one user would be using this at any given time, so no need for store user token, 
 * Based on the above a local/session storage would do to persist user data
 */